const axios = require('axios');
const fs = require('fs');
const path = 'steam_profile_data.json';

const config = require('./config.json');
const STEAM_API_KEY = config.token;
const STEAM_ID = config.steamId;

const getFormattedDate = () => {
  const date = new Date();
  return date.toISOString().split('T')[0]; // Format YYYY-MM-DD
};

const shouldFetchData = () => {
  try {
    if (!fs.existsSync(path)) return true;
    const existingData = JSON.parse(fs.readFileSync(path, 'utf8'));
    const lastRecordedDate = Object.keys(existingData.user?.totalPlaytime || {}).pop();
    return lastRecordedDate !== getFormattedDate();
  } catch (error) {
    console.error('Erreur lors de la lecture du fichier JSON:', error);
    return true;
  }
};

const getSteamProfileData = async () => {
  try {
    const response = await axios.get(`http://api.steampowered.com/ISteamUser/GetPlayerSummaries/v0002/?key=${STEAM_API_KEY}&steamids=${STEAM_ID}`);
    const profileData = response.data.response.players[0];
    
    const ownedGamesResponse = await axios.get(`http://api.steampowered.com/IPlayerService/GetOwnedGames/v0001/?key=${STEAM_API_KEY}&steamid=${STEAM_ID}&include_appinfo=1&include_played_free_games=1`);
    const ownedGamesData = ownedGamesResponse.data.response;
    profileData.ownedGames = ownedGamesData.games || [];
    
    const totalTimeResponse = await axios.get(`http://api.steampowered.com/IPlayerService/GetRecentlyPlayedGames/v0001/?key=${STEAM_API_KEY}&steamid=${STEAM_ID}`);
    const totalTimeData = totalTimeResponse.data.response;
    const totalTime = (totalTimeData.games || []).reduce((acc, game) => acc + game.playtime_forever, 0);
    
    profileData.total_playtime = { total: totalTime, windows: 0, linux: 0 };
    profileData.ownedGames.forEach((game) => {
      profileData.total_playtime.windows += game.playtime_windows_forever || 0;
      profileData.total_playtime.linux += game.playtime_linux_forever || 0;
    });
    
    return profileData;
  } catch (error) {
    console.error('Erreur lors de la récupération des données Steam:', error);
    throw error;
  }
};

const saveSteamProfileDataToJSON = async () => {
  if (!shouldFetchData()) {
    console.log(`Les données ont déjà été enregistrées aujourd'hui.`);
    return;
  }
  
  try {
    let steamProfileData = {};
    if (fs.existsSync(path)) {
      steamProfileData = JSON.parse(fs.readFileSync(path, 'utf8'));
    }
    
    const profileData = await getSteamProfileData();
    const currentDate = getFormattedDate();
    
    steamProfileData.user = {
      name: profileData.personaname,
      avatar: profileData.avatar,
      steamid: profileData.steamid,
      totalPlaytime: {
        ...steamProfileData.user?.totalPlaytime,
        [currentDate]: profileData.total_playtime
      }
    };
    
    steamProfileData.games = steamProfileData.games || {};
    profileData.ownedGames.forEach((game) => {
      const gameId = game.appid;
      if (!steamProfileData.games[gameId]) {
        steamProfileData.games[gameId] = { 
          name: game.name, 
          img_icon_url: `http://cdn.akamai.steamstatic.com/steamcommunity/public/images/apps/${gameId}/${game.img_icon_url}.jpg`, 
          gametime: {} 
        };
      }
      steamProfileData.games[gameId].gametime[currentDate] = {
        playtime_forever: game.playtime_forever,
        windowsPlaytime: game.playtime_windows_forever || 0,
        linuxPlaytime: game.playtime_linux_forever || 0
      };
    });
    
    fs.writeFileSync(path, JSON.stringify(steamProfileData, null, 2));
    console.log('Données du profil Steam mises à jour et enregistrées.');
  } catch (error) {
    console.error('Erreur lors de l\'enregistrement des données:', error);
  }
};

saveSteamProfileDataToJSON();
