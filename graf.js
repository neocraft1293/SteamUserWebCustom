const express = require('express');
const fs = require('fs');
const app = express();
const Chart = require('chart.js');

//declare les fonctions du script

//fonction pour recuperer tout les dates de la base de donnée
//et gere en cas de doublon
function getDates(games) {
    const dates = new Set();
    for (const gameId in games) {
        if (Object.hasOwnProperty.call(games, gameId)) {
            const game = games[gameId];
            for (const dateStr in game.gametime) {
                dates.add(dateStr);
            }
        }
    }
    return Array.from(dates).sort();
}

//aplelle de la fonction getDates pour la metre dans la console
fs.readFile('steam_profile_data.json', 'utf8', (err, data) => {
    if (err) {
        console.error("Une erreur s'est produite lors de la lecture du fichier JSON:", err);
        return;
    }
    const jsonData = JSON.parse(data);
    const dates = getDates(jsonData.games);
    console.log("Dates de jeu disponibles:", dates);
});





//page d'accueil avec une page html
app.get('/', (req, res) => {
    res.send(`
        <!DOCTYPE html>
        <html lang="en">
        <head>
            <meta charset="UTF-8">
            <meta name="viewport" content="width=device-width, initial-scale=1.0">
            <title>Comparaison du temps de jeu</title>
        </head>
        <body>
            <h1>page custom steam</h1>

            <p> 
                <a href="/neocraft1293">Voir le profile de neocraft1293</a>
            </p>

            <p>
                <a href="/graphique">Voir le graphique de comparaison du temps de jeu</a>
            </p>
            <p>
                <a href="/graphiqueTotal">Voir le graphique de comparaison du temps de jeu total</a>
            </p>
            <p>
                <a href="/graphiqueLine">Voir le graphique de comparaison du temps de jeu total sur Windows et Linux au fil du temps</a>
            </p>
            <p>
                <a href="/graphiqueJeux">Voir le graphique de comparaison des temps de jeu par jeu</a>
            </p>
            <p>
                <a href="/telecharger">Télécharger le fichier JSON</a>
            </p>
	    <p>
                <a href="https://codeberg.org/neocraft1293/SteamUserWebCustom">voir le code source</a>
            </p>
        </body>
        </html>
    `);
});



//page pour telecharger le fichier JSON
app.get('/telecharger', (req, res) => {
    // Lire les données de jeu à partir du fichier JSON
    fs.readFile('steam_profile_data.json', 'utf8', (err, data) => {
        if (err) {
            console.error("Une erreur s'est produite lors de la lecture du fichier JSON:", err);
            res.status(500).send('Erreur lors de la lecture des données.');
            return;
        }
        // Renvoyer le fichier JSON en tant que réponse
        res.set('Content-Type', 'application/json');
        res.set('Content-Disposition', 'attachment; filename=steam_profile_data.json');
        res.send(data);
    });
});


// Définir une route pour afficher la page web avec le graphique
app.get('/graphique', (req, res) => {
    // Lire les données de jeu à partir du fichier JSON
    fs.readFile('steam_profile_data.json', 'utf8', (err, data) => {
        if (err) {
            console.error("Une erreur s'est produite lors de la lecture du fichier JSON:", err);
            res.status(500).send('Erreur lors de la lecture des données.');
            return;
        }

        try {
            const jsonData = JSON.parse(data);
            const { games } = jsonData;

            const currentDate = new Date();
            let closestDate = null;

            // Trouver la date de jeu la plus proche de la date actuelle
            for (const gameId in games) {
                if (Object.hasOwnProperty.call(games, gameId)) {
                    const game = games[gameId];
                    for (const dateStr in game.gametime) {
                        const date = new Date(dateStr);
                        if (!closestDate || Math.abs(date - currentDate) < Math.abs(closestDate - currentDate)) {
                            closestDate = date;
                        }
                    }
                }
            }
            //la varibale de la date la plus proche est declarée avec la variable closestDate
            

            if (!closestDate) {
                res.status(500).send('Aucune donnée de temps de jeu disponible.');
                return;
            }
            else{
                console.log("La date la plus proche est: ", closestDate);
            }   

            let jeuLabels = [];
            let tempsJeuWindows = [];
            let tempsJeuLinux = [];

            // Extraire les temps de jeu correspondants à la date la plus proche
            for (const gameId in games) {
                if (Object.hasOwnProperty.call(games, gameId)) {
                    const game = games[gameId];
                    const gameTime = game.gametime[closestDate.toISOString()];
                    if (gameTime && (gameTime.windowsPlaytime + gameTime.linuxPlaytime > 0)) { 
                        jeuLabels.push(game.name);
                        tempsJeuWindows.push(gameTime.windowsPlaytime / 60); // Convertir en heures
                        tempsJeuLinux.push(gameTime.linuxPlaytime / 60); // Convertir en heures
                    }
                }
            }

            // Combiner les temps de jeu Windows et Linux pour obtenir le temps total de jeu
            const tempsJeuTotal = tempsJeuWindows.map((value, index) => value + tempsJeuLinux[index]);

            // Trier les jeux par temps de jeu total
            const sortedIndexes = tempsJeuTotal.map((value, index) => index).sort((a, b) => tempsJeuTotal[b] - tempsJeuTotal[a]);
            jeuLabels = sortedIndexes.map(index => jeuLabels[index]);
            tempsJeuWindows = sortedIndexes.map(index => tempsJeuWindows[index]);
            tempsJeuLinux = sortedIndexes.map(index => tempsJeuLinux[index]);

            // Créer un graphique
            const chart = createChart(jeuLabels, tempsJeuWindows, tempsJeuLinux);

            // Rendre une page HTML avec le graphique
            res.send(`
                <!DOCTYPE html>
                <html lang="en">
                <head>
                    <meta charset="UTF-8">
                    <meta name="viewport" content="width=device-width, initial-scale=1.0">
                    <title>Comparaison du temps de jeu</title>
                    <script src="https://cdn.jsdelivr.net/npm/chart.js"></script>
                </head>
                <body>
                    <h1>Comparaison du temps de jeu</h1>
                    <canvas id="myChart" width="800" height="400"></canvas>
                    <script>
                        ${chart}
                    </script>
                    <p>
                        <a href="/">Retour à la page d'accueil</a>
                    </p>
                </body>
                </html>
            `);
        } catch (error) {
            console.error("Une erreur s'est produite lors de l'analyse des données JSON:", error);
            res.status(500).send('Erreur lors de l\'analyse des données JSON.');
        }
    });
});

// Fonction pour créer un graphique de comparaison des temps de jeu en ligne
function createChart(jeuLabels, tempsJeuWindows, tempsJeuLinux) {
    const data = {
        labels: jeuLabels,
        datasets: [{
            label: 'Temps de jeu sur Windows (heures)',
            backgroundColor: 'rgba(255, 99, 132, 0.5)',
            borderColor: 'rgba(255, 99, 132, 1)',
            borderWidth: 1,
            data: tempsJeuWindows,
        }, {
            label: 'Temps de jeu sur Linux (heures)',
            backgroundColor: 'rgba(54, 162, 235, 0.5)',
            borderColor: 'rgba(54, 162, 235, 1)',
            borderWidth: 1,
            data: tempsJeuLinux,
        }]
    };
    const config = {
        type: 'bar',
        data: data,
        options: {
            scales: {
                y: {
                    beginAtZero: true
                }
            }
        }
    };
    return `new Chart(document.getElementById('myChart').getContext('2d'), ${JSON.stringify(config)});`;
}




//cree un graphique avec les temp de jeu sur total
//cree la focntion pour le graphique pour le temps de jeu total
// Fonction pour créer un graphique de comparaison des temps de jeu total
// Fonction pour créer un graphique de comparaison des temps de jeu total
function createChartTotal(jeuLabels, tempsJeuTotal) {
    // Créer une copie des tableaux de labels et de temps de jeu total
    const jeuLabelsSorted = [];
    const tempsJeuTotalSorted = [];

    // Filtrer les jeux avec un temps de jeu total non nul
    for (let i = 0; i < tempsJeuTotal.length; i++) {
        if (tempsJeuTotal[i] > 0) {
            jeuLabelsSorted.push(jeuLabels[i]);
            tempsJeuTotalSorted.push(tempsJeuTotal[i]);
        }
    }
    
    // Trier les jeux par temps de jeu total
    for (let i = 0; i < tempsJeuTotalSorted.length - 1; i++) {
        for (let j = i + 1; j < tempsJeuTotalSorted.length; j++) {
            if (tempsJeuTotalSorted[i] < tempsJeuTotalSorted[j]) {
                // Échanger les éléments
                [tempsJeuTotalSorted[i], tempsJeuTotalSorted[j]] = [tempsJeuTotalSorted[j], tempsJeuTotalSorted[i]];
                [jeuLabelsSorted[i], jeuLabelsSorted[j]] = [jeuLabelsSorted[j], jeuLabelsSorted[i]];
            }
        }
    }

    // Créer le graphique avec les données triées
    const data = {
        labels: jeuLabelsSorted,
        datasets: [{
            label: 'Temps de jeu total (heures)',
            backgroundColor: 'rgba(75, 192, 192, 0.5)',
            borderColor: 'rgba(75, 192, 192, 1)',
            borderWidth: 1,
            data: tempsJeuTotalSorted,
        }]
    };
    const config = {
        type: 'bar',
        data: data,
        options: {
            scales: {
                y: {
                    beginAtZero: true
                }
            }
        }
    };
    return `new Chart(document.getElementById('myChart').getContext('2d'), ${JSON.stringify(config)});`;
}


//cree une page pour le graphique avec les temps de jeu total

// Définir une route pour afficher la page web avec le graphique des temps de jeu total
app.get('/graphiqueTotal', (req, res) => {
    // Lire les données de jeu à partir du fichier JSON
    fs.readFile('steam_profile_data.json', 'utf8', (err, data) => {
        if (err) {
            console.error("Une erreur s'est produite lors de la lecture du fichier JSON:", err);
            res.status(500).send('Erreur lors de la lecture des données.');
            return;
        }

        try {
            const jsonData = JSON.parse(data);
            const { games } = jsonData;

            const currentDate = new Date();
            let closestDate = null;

            // Trouver la date de jeu la plus proche de la date actuelle
            for (const gameId in games) {
                if (Object.hasOwnProperty.call(games, gameId)) {
                    const game = games[gameId];
                    for (const dateStr in game.gametime) {
                        const date = new Date(dateStr);
                        if (!closestDate || Math.abs(date - currentDate) < Math.abs(closestDate - currentDate)) {
                            closestDate = date;
                        }
                    }
                }
            }

            if (!closestDate) {
                res.status(500).send('Aucune donnée de temps de jeu disponible.');
                return;
            }

            let jeuLabels = [];
            let tempsJeuTotal = [];
            
            // Extraire les temps de jeu correspondants à la date la plus proche
            for (const gameId in games) {
                if (Object.hasOwnProperty.call(games, gameId)) {
                    const game = games[gameId];
                    const gameTime = game.gametime[closestDate.toISOString()];
                    if (gameTime && (gameTime.windowsPlaytime + gameTime.linuxPlaytime > 0)) { 
                        jeuLabels.push(game.name);
                        tempsJeuTotal.push((gameTime.windowsPlaytime + gameTime.linuxPlaytime) / 60); // Convertir en heures
                    }
                }
            }

            // Créer un graphique
            const chart = createChartTotal(jeuLabels, tempsJeuTotal);

            // Rendre une page HTML avec le graphique
            res.send(`
                <!DOCTYPE html>
                <html lang="en">
                <head>
                    <meta charset="UTF-8">
                    <meta name="viewport" content="width=device-width, initial-scale=1.0">
                    <title>Comparaison du temps de jeu total</title>
                    <script src="https://cdn.jsdelivr.net/npm/chart.js"></script>
                </head>
                <body>
                    <h1>Comparaison du temps de jeu total</h1>
                    <canvas id="myChart" width="800" height="400"></canvas>
                    <script>
                        ${chart}
                    </script>
                    <p>
                        <a href="/">Retour à la page d'accueil</a>
                    </p>
                </body>
                </html>
            `);
        } catch (error) {
            console.error("Une erreur s'est produite lors de l'analyse des données JSON:", error);
            res.status(500).send('Erreur lors de l\'analyse des données JSON.');
        }
    });
});





// Fonction pour créer un graphique de comparaison des temps de jeu total sur Windows et Linux au fil du temps avec les dates en bas
function createChartLine(totalPlaytime) {
    const jeuLabels = Object.keys(totalPlaytime);
    const tempsJeuTotalWindows = [];
    const tempsJeuTotalLinux = [];

    jeuLabels.forEach(date => {
        const playtime = totalPlaytime[date];
        tempsJeuTotalWindows.push(playtime.windows / 60); // Convertir en heures
        tempsJeuTotalLinux.push(playtime.linux / 60); // Convertir en heures
    });

    const data = {
        labels: jeuLabels,
        datasets: [{
            label: 'Temps de jeu total sur Windows (heures)',
            backgroundColor: 'rgba(255, 99, 132, 0.5)',
            borderColor: 'rgba(255, 99, 132, 1)',
            borderWidth: 1,
            data: tempsJeuTotalWindows,
        }, {
            label: 'Temps de jeu total sur Linux (heures)',
            backgroundColor: 'rgba(54, 162, 235, 0.5)',
            borderColor: 'rgba(54, 162, 235, 1)',
            borderWidth: 1,
            data: tempsJeuTotalLinux,
        }]
    };

    const config = {
        type: 'line',
        data: data,
        options: {
            scales: {
                x: {
                    display: true,
                    title: {
                        display: true,
                        text: 'Date'
                    }
                },
                y: {
                    beginAtZero: true,
                    title: {
                        display: true,
                        text: 'Temps de jeu total (heures)'
                    }
                }
            }
        }
    };

    return `new Chart(document.getElementById('myChart').getContext('2d'), ${JSON.stringify(config)});`;
}

// Lire les données de jeu à partir du fichier JSON
app.get('/graphiqueLine', (req, res) => {
    fs.readFile('steam_profile_data.json', 'utf8', (err, data) => {
        if (err) {
            console.error("Une erreur s'est produite lors de la lecture du fichier JSON:", err);
            res.status(500).send('Erreur lors de la lecture des données.');
            return;
        }

        try {
            const jsonData = JSON.parse(data);
            const totalPlaytime = jsonData.user.totalPlaytime;

            // Créer un graphique
            const chart = createChartLine(totalPlaytime);

            // Rendre une page HTML avec le graphique
            res.send(`
                <!DOCTYPE html>
                <html lang="en">
                <head>
                    <meta charset="UTF-8">
                    <meta name="viewport" content="width=device-width, initial-scale=1.0">
                    <title>Comparaison du temps de jeu total</title>
                    <script src="https://cdn.jsdelivr.net/npm/chart.js"></script>
                </head>
                <body>
                    <h1>Comparaison du temps de jeu total</h1>
                    <canvas id="myChart" width="800" height="400"></canvas>
                    <script>
                        ${chart}
                    </script>
                    <p>
                        <a href="/">Retour à la page d'accueil</a>
                    </p>
                </body>
                </html>
            `);
        } catch (error) {
            console.error("Une erreur s'est produite lors de l'analyse des données JSON:", error);
            res.status(500).send('Erreur lors de l\'analyse des données JSON.');
        }
    });
});







/* 
la stuctuire du fichier json
{
  "user": {
    "name": "Nom de l'utilisateur",
    "avatar": "URL de l'avatar",
    "steamid": "ID Steam de l'utilisateur",
    "totalPlaytime": {
      "2024-02-19T12:00:00.000Z": {
        "total": 1500,
        "windows": 1000,
        "linux": 500
      }
      // Autres dates de temps de jeu peuvent être ajoutées ici
    }
  },
  "games": {
    "730": {
      "name": "Counter-Strike: Global Offensive",
      "img_icon_url": "URL de l'icône du jeu",
      "gametime": {
        "2024-02-19T12:00:00.000Z": {
          "playtime_forever": 500,
          "windowsPlaytime": 300,
          "linuxPlaytime": 200
        }
        // Autres dates de temps de jeu peuvent être ajoutées ici
      }
    },
    // Autres jeux peuvent être ajoutés ici
  }
}
*/







































































// Fonction pour créer un graphique des temps de jeu par jeu
function createChartJeux(jeuxData) {
    const jeuLabels = Object.keys(jeuxData);
    const dates = getDates(jeuxData); // Fonction pour obtenir les dates du jeu
    const datasets = [];

    // Créer un ensemble de données pour chaque jeu
    jeuLabels.forEach(jeuId => {
        const jeu = jeuxData[jeuId];
        const tempsJeuJeu = [];

        // Remplir le tableau avec le temps de jeu pour chaque date
        dates.forEach(date => {
            const gameTime = jeu.gametime[date];
            tempsJeuJeu.push(gameTime ? (gameTime.windowsPlaytime + gameTime.linuxPlaytime) / 60 : 0); // Convertir en heures
        });

        // Vérifier si le jeu a toujours un temps de jeu nul
        if (!tempsJeuJeu.every(time => time === 0)) {
            datasets.push({
                label: jeu.name,
                data: tempsJeuJeu,
            });
        }
    });

    const data = {
        labels: dates,
        datasets: datasets,
    };

    const config = {
        type: 'line',
        data: data,
        options: {
            scales: {
                y: {
                    beginAtZero: true
                }
            }
        }
    };

    return `new Chart(document.getElementById('myChart').getContext('2d'), ${JSON.stringify(config)});`;
}

// Fonction pour récupérer toutes les dates de jeu
function getDates(jeuxData) {
    const dates = new Set();
    Object.values(jeuxData).forEach(jeu => {
        Object.keys(jeu.gametime).forEach(date => {
            dates.add(date);
        });
    });
    return Array.from(dates).sort();
}

// Route pour afficher le graphique des temps de jeu par jeu
app.get('/graphiqueJeux', (req, res) => {
    // Lire les données de jeu à partir du fichier JSON
    fs.readFile('steam_profile_data.json', 'utf8', (err, data) => {
        if (err) {
            console.error("Une erreur s'est produite lors de la lecture du fichier JSON:", err);
            res.status(500).send('Erreur lors de la lecture des données.');
            return;
        }

        try {
            const jsonData = JSON.parse(data);
            const jeuxData = jsonData.games;

            // Créer un graphique
            const chart = createChartJeux(jeuxData);

            // Rendre une page HTML avec le graphique
            res.send(`
                <!DOCTYPE html>
                <html lang="en">
                <head>
                    <meta charset="UTF-8">
                    <meta name="viewport" content="width=device-width, initial-scale=1.0">
                    <title>Comparaison des temps de jeu par jeu</title>
                    <script src="https://cdn.jsdelivr.net/npm/chart.js"></script>
                </head>
                <body>
                    <h1>Comparaison des temps de jeu par jeu</h1>
                    <canvas id="myChart" width="1000" height="600"></canvas>
                    <script>
                        ${chart}
                    </script>
                    <p>
                        <a href="/">Retour à la page d'accueil</a>
                    </p>
                </body>
                </html>
            `);
        } catch (error) {
            console.error("Une erreur s'est produite lors de l'analyse des données JSON:", error);
            res.status(500).send('Erreur lors de l\'analyse des données JSON.');
        }
    });
});



































































































































































































































































































































































































































































































































// Gérer les erreurs 404 avec un peit jeu de mots et un lien vers la page d'accueil
app.use((req, res) => {
    res.status(404).send(`
        <h1>Erreur 404</h1>
        <p>Désolé, la page demandée est introuvable.</p>
        <p>
            <a href="/neocraft1293">Retour à la page d'accueil</a>
        </p>
    `);
});

// Démarrer le serveur
const PORT = process.env.PORT || 3001;
app.listen(PORT, () => {
    console.log(`Serveur démarré sur le port ${PORT}`);
});
